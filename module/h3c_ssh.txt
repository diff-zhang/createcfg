{%- if ssh is defined %}
public-key local create rsa	
ssh server enable	
line vty 0 4	
 authentication-mode scheme	
 quit	
ssh user {{ssh.username}} service-type stelnet authentication-type password	
local-user {{ssh.username}}	
 password simple {{ssh.password}}
 service-type ssh telnet
 authorization-attribute user-role network-admin	
 quit	
{%- endif %}
